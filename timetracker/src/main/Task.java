package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Represents an activity that elapses on different moments of time
 *
 */
public class Task extends Node implements Observer{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1493146476544948212L;
	
	static Logger logger = LoggerFactory.getLogger(Task.class);
	
	/*
	 * (non-Javadoc)
	 * @see main.Node#getTime()
	 */
	public double getTime() {
	
		double time = 0;
		Interval aux;
		for(int i=0;i<this.intervals.size();i++){
			aux = this.intervals.get(i);
			time = time + aux.getTranscurredTime();
		}
		return time;
	}

	/**
	 * @uml.property  name="active" readOnly="true"
	 */
	private boolean active = false;

	/**
	 * Getter of the property <tt>active</tt>
	 * @return  Returns the active.
	 * @uml.property  name="active"
	 */
	public boolean isActive() {
		return active;
	}

	/** 
	 * @uml.property name="intervals"
	 * @uml.associationEnd readOnly="true" multiplicity="(0 -1)" ordering="true" aggregation="composite" inverse="task:main.Interval"
	 */
	private List<Interval> intervals;

	/** 
	 * Getter of the property <tt>intervals</tt>
	 * @return  Returns the intervals.
	 * @uml.property  name="intervals"
	 */
	public List<Interval> getIntervals() {
		return intervals;
	}

		
	/**
	 * Creates a new interval of time, and turns the task active
	 */
	public void startTimer(){
		
		if(this.active==true){
			logger.warn("La tasca "+this.getName()+" ja ha estat iniciada");
		}
		else{
		logger.info("La tasca "+this.getName()+" s'ha iniciat");
		Clock aux = Clock.getInstance();
		aux.addObserver(this);
		Calendar actualDate = aux.getDate();
		Interval newInterval = new Interval(actualDate);
		this.intervals.add(newInterval);
		this.active = true;
		}
		
	}


			
	/**
	 * Task stops being activated and stops the current interval
	 */
	public void stopTimer(){
		
		if(this.active==false){
			logger.warn("La tasca "+this.getName()+" no ha estat iniciada");
		}
		else{
			logger.info("La tasca "+this.getName()+" s'ha parat");
			this.active = false;
			Clock aux = Clock.getInstance();
			aux.deleteObserver(this);
			
			int size = this.intervals.size();
			Interval activeInterval = this.intervals.get(size-1);
			if(activeInterval.getTranscurredTime()==0){
				this.intervals.remove(activeInterval);
			}
		}
		
	}

	/**
	 * Refresh the elapsed time and the final time of the task
	 */
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		Clock clock = Clock.getInstance();
		final double timeUnit = clock.getRefreshTime();
		
		int size = this.intervals.size();
		Interval activeInterval = this.intervals.get(size-1);
		activeInterval.addTime(timeUnit);
		activeInterval.setEndTime(clock.getDate());
		logger.debug("In "+clock.getDate().getTime().toString()+" Task"+this.getName()+":Added "+timeUnit+" seconds");
	}


				
	/**
	 * Task constructor
	 * @param name of the task
	 * @param description of the task 
	 */
	public Task(String name, String description){
		super(name,description);
		this.intervals = new ArrayList<Interval>();
	}


		
	/*
	 * (non-Javadoc)
	 * @see main.Node#print()
	 */
	public String print(){
		
		String taskString = "";
		String nom = this.getName();
		long sec = (long)this.getTime();
		String time = String.format("%02d:%02d:%02d", 
				TimeUnit.SECONDS.toHours(sec), //hores
				TimeUnit.SECONDS.toMinutes(sec)-TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(sec)),
				sec-TimeUnit.SECONDS.toMinutes(sec));
		String startTime = "";
		String endTime = "";
		
		if(this.intervals.size()>0){
			int lastIntervalIndex = this.intervals.size()-1;
			Interval firstInterval = this.getIntervals().get(0);
			Interval lastInterval = this.getIntervals().get(lastIntervalIndex);
			startTime = firstInterval.getStartTime().getTime().toString();
			endTime = lastInterval.getEndTime().getTime().toString();
		}
		
		taskString = nom+"\t"+startTime+"\t"+endTime+"\t"+time+"\n";
		return taskString;
		
	}


	@Override
	/*
	 * (non-Javadoc)
	 * @see main.Node#getStartTime()
	 */
	public Calendar getStartTime() {
		Calendar starTime = null;
		if(this.intervals.size()>0){
			Interval firstInterval = this.getIntervals().get(0);
			starTime = firstInterval.getStartTime();
		}
		return starTime;
	}


	@Override
	/*
	 * (non-Javadoc)
	 * @see main.Node#getEndTime()
	 */
	public Calendar getEndTime() {
		Calendar endTime = null;
		if(this.intervals.size()>0){
			int lastIntervalIndex = this.intervals.size()-1;
			Interval lastInterval = this.getIntervals().get(lastIntervalIndex);
			endTime = lastInterval.getEndTime();
		}
		return endTime;
	}

}
