package main;

import java.util.Calendar;
import java.util.Observable;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Brings the current time information 
 * and sends an update to all the active tasks 
 * in order to count the elapsed time
 */
public class Clock extends Observable {
		
		static Logger logger = LoggerFactory.getLogger(Clock.class);
		
		/**
		 * Is the responsible of calling the tick() function each period of time
		 * in order to call of the active task for a time update
		 *
		 */
		public class ClockTimer extends Thread {
			
			/*
			 * (non-Javadoc)
			 * @see java.lang.Thread#run()
			 */
			public void run(){
				Clock clock = Clock.getInstance();
				while(true){
					try {
						clock.tick();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}


		/**
		 * @uml.property  name="instance"
		 */
		private static Clock instance = null;
		/**
		 * @uml.property  name="refreshTime" readOnly="true"
		 */
		private static long refreshTime = 1;
		
		/**
		 * Clock constructor 
		 * It creates a instance of Clock Timer in order to
		 * call to all active tasks every elapsed refresh period 
		 */
		private Clock(){
			logger.debug("Timer started");
			ClockTimer timer = new ClockTimer();
			timer.start(); 
			
		}
		
		/**
		 * Returns an existing instance of clock if already exist
		 * or creates a new one if there isn't any instance. 
		 */
		public static Clock getInstance(){
			if(instance == null){
				instance = new Clock();
			}
			return instance;
		}
		
		
		/**
		 * @return time info (Calendar instance) from the time moment
		 * this function is called
		 */
		public Calendar getDate(){
			Calendar actualDay = Calendar.getInstance();
			return actualDay;
		}

			
		/**
		 * Calls every active task in order to count it's elapsed time
		 * @throws InterruptedException 
		 */
		public void tick() throws InterruptedException{
			TimeUnit.SECONDS.sleep(refreshTime);
			setChanged();
			notifyObservers();
		}

			
		/**
		 * Getter for the property refreshTime
		 * @return time period to wait to call the tick method 
		 */
		public long getRefreshTime(){
			return Clock.refreshTime;
		}

			
		/**
		 * Setter for the property refreshTime
		 * @param time period to wait to call the tick method 
		 */
		public void setRefreshTime(long refreshTime){
			Clock.refreshTime = refreshTime;
		}
		











				
				


					
		





}
