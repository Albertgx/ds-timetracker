package main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.util.concurrent.TimeUnit;


public class Client {
	
	/**
	 * @param args
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws InterruptedException, FileNotFoundException, IOException, ClassNotFoundException {
		

		testA1();
		/*Project root = loadTree();
		TaskManager taskManager = new TaskManager(root);
		taskManager.printTable();*/
		
	
	}
	public static void testA2() throws InterruptedException, FileNotFoundException, IOException{
			
		Project root = new Project("root","");
		TaskManager taskManager = new TaskManager(root);
		Clock clock = Clock.getInstance();
		clock.setRefreshTime(2);
		
		Project explorer;
		explorer = taskManager.getRoot();
		explorer.newProject("P1", "");
		explorer = (Project)explorer.getSubProject(0);
		explorer.newTask("T3", "");
		explorer.newProject("P2", "");
		explorer = (Project)explorer.getSubProject(1);
		explorer.newTask("T1", "");
		explorer.newTask("T2", "");
		
		explorer = taskManager.getRoot();
		explorer = (Project)explorer.getSubProject(0);
		Task task3 = (Task)explorer.getSubProject(0);
		task3.startTimer();
		explorer = (Project)explorer.getSubProject(1);
		Task task2 = (Task)explorer.getSubProject(1);
		Task task1 = (Task)explorer.getSubProject(0);
		
		TimeUnit.SECONDS.sleep(4);
		
		task2.startTimer();
		
		TimeUnit.SECONDS.sleep(2);
		
		task3.stopTimer();
		
		TimeUnit.SECONDS.sleep(2);
		
		task1.startTimer();
		
		TimeUnit.SECONDS.sleep(4);
		
		task1.stopTimer();
		
		TimeUnit.SECONDS.sleep(2);
		
		task2.stopTimer();
		
		TimeUnit.SECONDS.sleep(4);
		
		task3.startTimer();
		
		TimeUnit.SECONDS.sleep(2);
		
		task3.stopTimer();
		
		taskManager.printTable();
		saveTree(taskManager.getRoot());
	}
	
	public static void saveTree(Project root) throws FileNotFoundException, IOException{
		ObjectOutputStream objtectOutputStream = new ObjectOutputStream(new FileOutputStream("Tree.bin"));
		objtectOutputStream.writeObject(root);
	}
	
	public static Project loadTree() throws FileNotFoundException, IOException, ClassNotFoundException{
		ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("Tree.bin"));
		Project root = (Project) objectInputStream.readObject();
		
		return root;
	}
	
	public static void testA1() throws InterruptedException, FileNotFoundException, IOException{
		
		Project root = new Project("root","");
		TaskManager taskManager = new TaskManager(root);

		Project explorer;
		explorer = taskManager.getRoot();
		explorer.newProject("P1", "");
		explorer = (Project)explorer.getSubProject(0);
		explorer.newTask("T3", "");
		explorer.newProject("P2", "");
		explorer = (Project)explorer.getSubProject(1);
		explorer.newTask("T1", "");
		explorer.newTask("T2", "");
		
		
		explorer = taskManager.getRoot();
		explorer = (Project)explorer.getSubProject(0);
		Task task3 = (Task)explorer.getSubProject(0);
		task3.startTimer();
		
		TimeUnit.SECONDS.sleep(3);
		
		task3.stopTimer();

		taskManager.printTable();
		
		
		TimeUnit.SECONDS.sleep(7);
		
		explorer = (Project) explorer.getSubProject(1);
		Task task2 = (Task)explorer.getSubProject(1);
		task2.startTimer();
		
		TimeUnit.SECONDS.sleep(10);
		
		task2.stopTimer();
		task3.startTimer();
		
		TimeUnit.SECONDS.sleep(2);
		
		task3.stopTimer();
		
		taskManager.printTable();
		
		saveTree(taskManager.getRoot());
		
		
	}
	
	public static void testExtra() throws InterruptedException{
		Project root = new Project("root","");
		TaskManager taskManager = new TaskManager(root);

		Project explorer;
		explorer = taskManager.getRoot();
		explorer.newProject("P1", "");
		explorer = (Project)explorer.getSubProject(0);
		explorer.newTask("T3", "");
		explorer.newProject("P2", "");
		explorer = (Project)explorer.getSubProject(1);
		explorer.newTask("T1", "");
		explorer.newTask("T2", "");
		
		explorer = taskManager.getRoot();
		explorer = (Project)explorer.getSubProject(0);
		Task task3 = (Task)explorer.getSubProject(0);
		for(int i=0;i<10;i++){
			task3.startTimer();
			TimeUnit.MILLISECONDS.sleep(200);
			task3.stopTimer();
		}
		
		taskManager.printTable();
		
	}

}
