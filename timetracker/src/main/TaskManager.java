package main;

import java.util.List;

/**
 * Contains all the tasks and projects 
 *
 */
public class TaskManager {

	/** 
	 * Contains all the subproject that client access
	 * @uml.property name="root"
	 * @uml.associationEnd readOnly="true" inverse="taskManager:main.Project"
	 */
	private Project root;

	/** 
	 * Getter of the property <tt>root</tt>
	 * @return  Returns the root.
	 * @uml.property  name="root"
	 */
	public Project getRoot() {
		return root;
	}
			
	/**
	 * TaskManager constructor
	 * @param root project that contains all subproject that client access
	 */
	public TaskManager(Project root){
		this.root = root;
	}

		
	/**
	 * Prints all the tasks and projects name,start time,end time and elapsed time created
	 */
	public void printTable(){
		System.out.println("Nom \t\t Temps inici \t\t\t Temps final \t\t Durada(hh:mm:ss)");
		System.out.println("----------------------------------------------------------------------------------");
		List<Node> subprojects = this.root.getSubprojects();
		
		String treeString = "";
		
		for(int i=0;i<subprojects.size();i++){
			treeString+=subprojects.get(i).print();
		}
		System.out.println(treeString);
		
	}

}
