package main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


import java.util.Calendar;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Client {
	
	static Logger logger = LoggerFactory.getLogger(Client.class);
	
	/**
	 * @param args
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws InterruptedException{
		
		testA1();
		testA2();
		menu();
	}
	
	/*
	 * Saves all the tree project in a binary file
	 */
	public static void saveTree(Project root){
		
		ObjectOutputStream objtectOutputStream;
		try {
			objtectOutputStream = new ObjectOutputStream(new FileOutputStream("Tree.bin"));
			objtectOutputStream.writeObject(root);
		} catch (IOException e) {
			logger.error("Error al crear el arxiu");
		}
	}
	
	/*
	 * Loads an existing tree in a binary file
	 */
	public static Project loadTree(){
		Project root = null;
		ObjectInputStream objectInputStream;
		
		try {
			objectInputStream = new ObjectInputStream(new FileInputStream("Tree.bin"));
			root = (Project) objectInputStream.readObject();
		} catch (IOException | ClassNotFoundException e) {
		
			logger.error("Error al llegir el arxiu");
		}
		return root;
	}
	
	
	public static void menu(){
		
		Scanner keyInput = new Scanner(System.in);
		boolean exit = false;
		int input = 1;
	
		String name = "";
		Task auxTask;
		Calendar scheduledTime = null;
		
		
		Project root = new Project("root","");
		Project actualProject;
		TaskManager taskManager = new TaskManager(root);
		root = taskManager.getRoot();
		actualProject = root;

		System.out.println("--- TIME TRACKER ---");
		
		while(!exit){
			System.out.println("1.Mostrar tasques/porjectes\n2.Seleccionar projecte\n" +
								"3.Afegir projecte\n4.Afegir tasca\n5.Afegir tasca(limit temps)\n" +
								"6.Afegir tasca(temps programat)\n7.Afegir tasca(totes els opcions)\n" +
								"8.Comen�ar tasca\n9.Detenir tasca\n0.Soritr\n");

			input = keyInput.nextInt();
			
		
			switch (input){
				case 1: taskManager.printTable();
				     	break;
				
				case 2: System.out.println("Selecciona un subprojecte (1-N) ");
						input = keyInput.nextInt();
						if(input == 0){
							actualProject = root;
						}
						else{
							try{
							actualProject = (Project) actualProject.getSubProject(input-1);
							}catch (Exception e){
								logger.error("No has seleccionat un projecte ");
							}
						}
						
				  		break;
				
				case 3: System.out.println("Introdueix el nom del projecte: ");
						name = keyInput.next();
						actualProject.newProject(name,"");
						break;
				
				case 4: System.out.println("Introdueix el nom de la tasca: ");
						name = keyInput.next();
						actualProject.newTask(name,"");
						break;
						
				case 5: System.out.println("Introdueix el nom de la tasca:");
						name = keyInput.next();
						System.out.println("Introdueix el limit de temps");
						input = keyInput.nextInt();
						actualProject.newTaskTimeLimit(name, "", input);
						break;
				case 6: System.out.println("Introdueix el nom de la tasca: ");
						name = keyInput.next();
						System.out.println("Introdueix en quans segons vols que comenci la tasca \n");
						input = keyInput.nextInt();
						
						scheduledTime = Calendar.getInstance();
						scheduledTime.add(Calendar.SECOND,input);
						
						actualProject.newTaskScheduledTime(name, "", scheduledTime);
						break;
				case 7: System.out.println("Introdueix el nom de la tasca: ");
						name = keyInput.next();
						System.out.println("Introdueix en quans segons vols que comenci la tasca ");
						input = keyInput.nextInt();
						System.out.println("Introdueix el temps limit");
						int tempsLimit = keyInput.nextInt();
						
					
						scheduledTime = Calendar.getInstance();
						scheduledTime.add(Calendar.SECOND,input);
						
						actualProject.newTaskScheduledTimeLimit(name, "", (double)tempsLimit, scheduledTime);
						break;
			
						
				case 8: System.out.println("Selecciona una subtasca (1-N)");
				  		input = keyInput.nextInt();
				  		try{
				  			auxTask = (Task)actualProject.getSubProject(input-1);
				  			auxTask.startTimer();
				  		}catch (Exception e){
				  			logger.error("No has seleccionat una tasca");
				  		}
				  			
				  		
					    break; 
					    
				case 9: System.out.println("Selecciona una subtasca (1-N)");
				  		input = keyInput.nextInt();
						try{
							auxTask = (Task)actualProject.getSubProject(input-1);
							auxTask.stopTimer();
						}catch (Exception e){
							logger.error("No has seleccionat una tasca");
						}
						
					    break; 
				 
				case 0: exit = true;
						break;
						
				default:
						logger.error("Opci� inv�lida. Introdueix una opci� v�lida.");
						break;
			}
			
		}
	}
	
	public static void testA1() throws InterruptedException{
		
		logger.info("S'ha iniciat el testA1");
		Project root = new Project("root","");
		TaskManager taskManager = new TaskManager(root);

		Project explorer;
		explorer = taskManager.getRoot();
		explorer.newProject("P1", "");
		explorer = (Project)explorer.getSubProject(0);
		explorer.newTask("T3", "");
		explorer.newProject("P2", "");
		explorer = (Project)explorer.getSubProject(1);
		explorer.newTask("T1", "");
		explorer.newTask("T2", "");
		
		
		explorer = taskManager.getRoot();
		explorer = (Project)explorer.getSubProject(0);
		Task task3 = (Task)explorer.getSubProject(0);
		task3.startTimer();
		
		TimeUnit.SECONDS.sleep(3);
		
		task3.stopTimer();

		taskManager.printTable();
		
		
		TimeUnit.SECONDS.sleep(7);
		
		explorer = (Project) explorer.getSubProject(1);
		Task task2 = (Task)explorer.getSubProject(1);
		task2.startTimer();
		
		TimeUnit.SECONDS.sleep(10);
		
		task2.stopTimer();
		task3.startTimer();
		
		TimeUnit.SECONDS.sleep(2);
		
		task3.stopTimer();
		
		System.out.println("--- TEST A1 ---\n");
		taskManager.printTable();
		
		saveTree(taskManager.getRoot());
		
	}
	
	
	public static void testA2() throws InterruptedException{
		
		logger.info("S'ha iniciat el testA2");
		Project root = new Project("root","");
		TaskManager taskManager = new TaskManager(root);
		Clock clock = Clock.getInstance();
		clock.setRefreshTime(2);
		
		Project explorer;
		explorer = taskManager.getRoot();
		explorer.newProject("P1", "");
		explorer = (Project)explorer.getSubProject(0);
		explorer.newTask("T3", "");
		explorer.newProject("P2", "");
		explorer = (Project)explorer.getSubProject(1);
		explorer.newTask("T1", "");
		explorer.newTask("T2", "");
		
		explorer = taskManager.getRoot();
		explorer = (Project)explorer.getSubProject(0);
		Task task3 = (Task)explorer.getSubProject(0);
		task3.startTimer();
		explorer = (Project)explorer.getSubProject(1);
		Task task2 = (Task)explorer.getSubProject(1);
		Task task1 = (Task)explorer.getSubProject(0);
		
		TimeUnit.SECONDS.sleep(4);
		
		task2.startTimer();
		
		TimeUnit.SECONDS.sleep(2);
		
		task3.stopTimer();
		
		TimeUnit.SECONDS.sleep(2);
		
		task1.startTimer();
		
		TimeUnit.SECONDS.sleep(4);
		
		task1.stopTimer();
		
		TimeUnit.SECONDS.sleep(2);
		
		task2.stopTimer();
		
		TimeUnit.SECONDS.sleep(4);
		
		task3.startTimer();
		
		TimeUnit.SECONDS.sleep(2);
		
		task3.stopTimer();
		
		System.out.println("--- TEST A2 ---\n");
		taskManager.printTable();
		saveTree(taskManager.getRoot());
	}
}
