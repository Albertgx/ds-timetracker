package main;

import java.util.Calendar;
import java.util.Observable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Wrapper for the BaseTask class that allows to set to
 * a task an scheduled time to be started 
 */
public class ScheduledTime extends TypeTask {

	/**
	 *Id used for the serialize implementation 
	 */
	private static final long serialVersionUID = 9021167698441879765L;

	static Logger logger = LoggerFactory.getLogger(ScheduledTime.class);
	
	/**
	 * Scheduled time for the task to start counting time
	 * @uml.property  name="startTime"
	 */
	private Calendar startTime;
	
	/**
	 * Indicates if the task can start according to the scheduled time
	 * @uml.property  name="canStart"
	 */
	private boolean canStart;

	/**
	 * ScheduledTime constructor
	 * @param startTime moment of time when the task must start
	 */
	public ScheduledTime(Task taskToAddFun,Calendar startTime) {
		
		super(taskToAddFun);
		super.setTopTask(this);
		this.startTime = startTime;
		this.canStart = false;
		
		Clock clock = Clock.getInstance();
		clock.addObserver(this);
	}
	
	/**
	 * Starts the timer only if the task has reached
	 * the scheduled time 
	 */
	public void startTimer(){
		if(canStart){
			super.startTimer();
		}
		else{
			logger.warn("La tasca no ha arribat al temps programat");
		}
	}
	

	
	/**
	 * Stops counting time
	 * That only can happen if the task has reached the scheduled time
	 */
	public void stopTimer(){
		if(canStart){
			super.stopTimer();
		}
		else{
			logger.warn("La tasca encara no ha pogut comen�ar");
		}
	}

	/**
	 * Checks if the task has reached the scheduled time and then,
	 * if it does, the method counts time as expected
	 */
	public void update(Observable arg0, Object arg1) {
		

		if(!canStart){
			Clock clock = Clock.getInstance();
			Calendar acutalDate = clock.getDate();
			if(this.startTime.before(acutalDate)){
				logger.info("La tasca programada "+super.getName()+" ha arribat a la hora programada");
				canStart = true;
				clock.deleteObserver(this);
				this.startTimer();
			}
		}
		if(canStart){
			super.update(arg0, arg1);
		}

	}
}