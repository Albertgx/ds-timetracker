package main;

import java.util.Calendar;
import java.util.Observable;

/**
 * Type task allows the subclasses to add functionality 
 * to a previous task encapsulated on this class
 * The functions that are not override by subclasses
 * are delegated to the encapsulated task 
 */
public abstract class TypeTask extends Task {
	
	/**
	 *Id for the serialize implementation  
	 */
	private static final long serialVersionUID = 8884098789401876014L;
	
	/**
	 * Task where will be added the new functionality
	 */
	protected Task taskToAddFun;
	
	/**
	 * TypeTask constructor
	 * @param taskToAddFun task where will be added the new functionality
	 */
	public TypeTask(Task taskToAddFun){
		
		this.taskToAddFun = taskToAddFun;
	
	}
	@Override
	public String getName(){
		return this.taskToAddFun.getName();
	}
	
	@Override
	public String getDescription(){
		return this.taskToAddFun.getDescription();
	}
	
	@Override
	public double getTime() {
		return this.taskToAddFun.getTime();
	}

	@Override
	public void startTimer() {
		this.taskToAddFun.startTimer();

	}

	@Override
	public void stopTimer() {
		this.taskToAddFun.stopTimer();

	}

	@Override
	public void update(Observable arg0, Object arg1) {
		this.taskToAddFun.update(arg0, arg1);

	}

	@Override
	public String print() {
		// TODO Auto-generated method stub
		return this.taskToAddFun.print();
	}

	@Override
	public  Calendar getStartTime() {
		
		return this.taskToAddFun.getStartTime();
	}

	@Override
	public Calendar getEndTime() {
		
		return this.taskToAddFun.getEndTime();
	}
	
	@Override
	public boolean isActive(){
		return this.taskToAddFun.isActive();
	}
	
	@Override 
	public void setTopTask(Task topTask){
		this.taskToAddFun.setTopTask(topTask);
	}
}