package main;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Represents a collection of tasks or projects as a one unique activity
 * that elapses on a different moments of time
 *
 */
public class Project extends Node {
	
	static Logger logger = LoggerFactory.getLogger(Project.class);
	/**
	 * Id for the serialize implementation 
	 */
	private static final long serialVersionUID = 8699152219902776968L;

	
	/** 
	 * Contains all the projects and tasks attached to this project
	 * @uml.property name="subprojects"
	 * @uml.associationEnd readOnly="true" multiplicity="(0 -1)" ordering="true" aggregation="shared" inverse="project:main.Node"
	 */
	private List<Node> subprojects;
	
	/**
	 * Project constructor
	 * @param name of the project
	 * @param description of the project 
	 */
	public Project(String name, String description){
		this.name = name;
		this.description = description;
		this.subprojects = new ArrayList<Node>();
	}
	
	
	
	/*
	 * (non-Javadoc)
	 * @see main.Node#getTime()
	 */
	public double getTime() {
		double time = 0;
		Node aux;
		for(int i=0;i<this.subprojects.size();i++){
			aux = this.subprojects.get(i);
			time = time + aux.getTime();
		}
		return time;
	}

	

	/** 
	 * Getter of the property <tt>subprojects</tt>
	 * @return  Returns the subprojects.
	 * @uml.property  name="subprojects"
	 */
	public List<Node> getSubprojects() {
		return subprojects;
	}

		
	/**
	 * Adds a new task to the subproject
	 * @param name of the new task
	 * @param description of the task
	 */
	public void newTask(String name, String description){
		
		Node newTask = new BaseTask(name,description);
		this.subprojects.add(newTask);
		logger.info("S'ha creat la tasca "+name);
	}
	
	/**
	 * Adds a new task with a limit of time to the subproject
	 * @param name of the new task
	 * @param timeLimit the maximum time the task can be active
	 * @param description of the task
	 */
	public void newTaskTimeLimit(String name, String description,double timeLimit){
		
		Node newTask = new TimeLimit(new BaseTask(name,description),timeLimit);
		this.subprojects.add(newTask);
		logger.info("S'ha creat la tasca " + name);
	}
	
	/**
	 * Adds a new task with a scheduled start to the subproject
	 * @param name of the new task
	 * @param scheduledTime moment of time for the task to start 
	 * @param description of the task
	 */
	public void newTaskScheduledTime(String name, String description,Calendar scheduledTime){
		
		
		Node newTask = new ScheduledTime(new BaseTask(name,description),scheduledTime);
		this.subprojects.add(newTask);
		logger.info("S'ha creat la tasca " + name);
	}
	
	/**
	 * Adds a new task with a limit of time and with a scheduled start, to the subproject
	 * @param name of the new task
	 * @param timeLimit the maximum time the task can be active
	 * @param scheduledTime moment of time for the task to start 
	 * @param description of the task
	 */
	public void newTaskScheduledTimeLimit(String name, String description,double timeLimit,Calendar scheduledTime){

		Node newTask = new TimeLimit(new ScheduledTime(new BaseTask(name,description),scheduledTime),timeLimit);
		this.subprojects.add(newTask);
		logger.info("S'ha creat la tasca " + name);
	}
			
	/**
	 * Adds a new project to the subproject
	 * @param name of the new project
	 * @param description of the new project
	 */
	public void newProject(String name, String description){
		Node newProject = new Project(name,description);
		this.subprojects.add(newProject);
		logger.info("S'ha creat el projecte " + name);
	}
	
	

	/**
	 * Gets one of the subproject attached to this projects
	 * @return the subproject
	 * @param index of the subproject 
	 */
	public Node getSubProject(int index){
		
		Node subNode = this.subprojects.get(index);
		return subNode;
	}


					
	/**
	 * Removes a task or a project form the subproject list
	 * @param index of the subproject to remove
	 */
	public void removeNode(int index){
		this.subprojects.remove(index);
		logger.info("S'ha eleiminat el node "+this.subprojects.remove(index));
	}

		
	/*
	 * (non-Javadoc)
	 * @see main.Node#print() 
	 */
	public String print(){
		
		String projectString = "";
		String nom = this.getName();
		long sec = (long)this.getTime();
		//Process to pass ss format to hh:m:ss format
		String time = String.format("%02d:%02d:%02d", 
				TimeUnit.SECONDS.toHours(sec), //hores
				TimeUnit.SECONDS.toMinutes(sec)-TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(sec)),
				sec-TimeUnit.SECONDS.toMinutes(sec));
		
		
		String startTimeStr = "";
		String endTimeStr = "";
		Calendar startTime = this.getStartTime();
		if(startTime!=null){
			startTimeStr = startTime.getTime().toString();
		}
		Calendar endTime = this.getEndTime();
		if(endTime!=null){
			endTimeStr = endTime.getTime().toString();
		}
		
		projectString = nom+"\t"+startTimeStr+"\t"+endTimeStr+"\t"+time +"\n";
		for(int i=0;i<this.subprojects.size();i++){
			Node node = this.getSubProject(i);
			projectString += node.print();
		}
		return projectString;	
	}


	@Override
	/*
	 * (non-Javadoc)
	 * @see main.Node#getStartTime()
	 */
	public Calendar getStartTime() {
		
		Calendar minStartTime = null;
		Node auxNode = null;
		Calendar auxStartTime = null;
		
		int size = this.subprojects.size();
		
		//Search the first moment a subtask or subproject started
		for(int i=0;i<size;i++){
			auxNode = this.subprojects.get(i);
			auxStartTime = auxNode.getStartTime();
			if(auxStartTime != null){
				if(minStartTime == null){
					minStartTime = auxStartTime;
				}
				else if(auxStartTime.before(minStartTime)){
					minStartTime = auxStartTime;
				}
			}
		}
		return minStartTime;
	}

	
	@Override
	/*
	 * (non-Javadoc)
	 * @see main.Node#getEndTime()
	 */
	public Calendar getEndTime() {
		
		Calendar maxEndTime = null;
		Node auxNode = null;
		Calendar auxEndTime = null;
		
		int size = this.subprojects.size();
		
		//Search the last moment a subtask or subproject end or is in progress
		for(int i=0;i<size;i++){
			auxNode = this.subprojects.get(i);
			auxEndTime = auxNode.getEndTime();
			if(auxEndTime != null){
				if(maxEndTime == null){
					maxEndTime = auxEndTime;
				}
				else if(auxEndTime.after(maxEndTime)){
					maxEndTime = auxEndTime;
				}
			}
		}
		return maxEndTime;
	}
}