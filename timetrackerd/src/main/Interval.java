package main;

import java.io.Serializable;
import java.util.Calendar;

/**
 * 
 * This class is used for counting the elapsed time from a 
 * moment we start the task until we stop it 
 *
 */
public class Interval implements Serializable{

	/**
	 * Id for the serialize implementation 
	 */
	private static final long serialVersionUID = 8813296402050486686L;
	
	/**
	 * @uml.property  name="startTime" readOnly="true"
	 */
	private Calendar startTime;

	/**
	 * @uml.property  name="endTime" readOnly="true"
	 */
	private Calendar endTime;
	

	/**
	 * @uml.property  name="transcurredTime" readOnly="true"
	 */
	private double transcurredTime = 0;
	
	/**
	 * Interval constructor
	 * @param startTime the time that interval starts counting time
	 */
	public Interval(Calendar startTime){
		this.startTime = startTime;
	}
	
	
	/**
	 * Getter of the property <tt>startTime</tt>
	 * @return  Returns the startTime.
	 * @uml.property  name="startTime"
	 */
	public Calendar getStartTime() {
		return startTime;
	}

	/**
	 * Getter of the property <tt>endTime</tt>
	 * @return  Returns the endTime.
	 * @uml.property  name="endTime"
	 */
	public Calendar getEndTime() {
		return endTime;
	}


	/**
	 * Setter of the property endTime
	 * @param endTime moment of time the interval has ended or the actual time if is active 
	 */
	public void setEndTime(Calendar endTime){
		this.endTime = endTime;
	}
	
	/**
	 * Getter of the property <tt>transcurredTime</tt>
	 * @return  Returns the transcurredTime.
	 * @uml.property  name="transcurredTime"
	 */
	public double getTranscurredTime() {
		return transcurredTime;
	}

					
			
	/**
	 * Add time to the elapsed time of the interval
	 * @param time amount of time to add to the elapsed time
	 */
	public void addTime(double time){
		this.transcurredTime = this.transcurredTime+time;
	}
}