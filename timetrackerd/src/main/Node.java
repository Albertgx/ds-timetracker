package main;

import java.io.Serializable;
import java.util.Calendar;


/**
 * It represents a time event that elapses on a different moments of time
 */
public abstract class Node implements Serializable{
	

	/**
	 * Id for the serialize implementation
	 */
	private static final long serialVersionUID = 5807456182294216543L;
	/**
	 * Name of the node
	 * @uml.property  name="name" readOnly="true"
	 */
	protected String name;


	/**
	 * Aditional information about the node
	 * @uml.property  name="description" readOnly="true"
	 */
	protected String description;
	
	
	/**
	 * Getter of the property <tt>name</tt>
	 * @return  Returns the name.
	 * @uml.property  name="name"
	 */
	public String getName() {
		return name;
	}


	/**
	 * Getter of the property <tt>description</tt>
	 * @return  Returns the description.
	 * @uml.property  name="description"
	 */
	public String getDescription() {
		return description;
	}

		
	/**
	 * @return  Elapsed time that this node have been active
	 */
	public abstract double getTime();


	
	
	/** 
	 * Prints the name of the node, start time, end time and elapsed time
	 */
	public abstract String print();
		


			
	/**
	 * @return First time the node starts
	 */
	public abstract Calendar getStartTime();


	
	/**
	 * @return Last time the node ends
	 */
	public abstract Calendar getEndTime();
}